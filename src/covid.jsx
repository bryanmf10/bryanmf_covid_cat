import React, { useState, useEffect } from "react";

import { Container, Table, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from "reactstrap";
import styled from "styled-components";

const IconoCentrado = styled.i`
    position: absolute;
    left: 0;
    right: 0;
    top: 200px;
    width: 100%;
`;
const Covid = () => {
    const [lista, setLista] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(false);
    const [dropdownOpen, setOpen] = useState(false);

    const [comarca, setComarca] = useState();

    const toggle = () => setOpen(!dropdownOpen);

    useEffect(() => {
        const comarcaEnc = encodeURIComponent(comarca).replace(/%20/g, "+");
        const apiUrl = `https://analisi.transparenciacatalunya.cat/resource/jvut-jxu8.json?nom=${comarcaEnc}`;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => {
                setLista(data);
                setLoading(false);
            })
            .catch((error) => setError(true));


    }, [comarca]);


    if (error) {
        return <h3>Se ha producido un error</h3>;
    }

    if (loading) {
        return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />
    }

    const filas = lista.sort((a, b) => (a.data_ini > b.data_ini) ? 1 : -1).map(el => {
        const dataIni = new Date(el.data_ini);
        const dataFin = new Date(el.data_fi);
        const taxa = el.taxa_casos_confirmat;
        const casos = el.casos_confirmat;

        return (
            <tr>
                <td>{dataIni.toLocaleDateString()}</td>
                <td>{dataFin.toLocaleDateString()}</td>
                <td>{taxa}</td>
                <td>{casos}</td>
            </tr>
        )
    })

    return (
        
        <Container>
            <br/>
            <br/>
            <h2>{comarca}</h2>
            <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle caret>
                    Comarca
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem onClick={() => setComarca('ALT CAMP')}>ALT CAMP</DropdownItem>
                    <DropdownItem onClick={() => setComarca('ALT EMPORDA')}>ALT EMPORDA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('ALT PENEDES')}>ALT PENEDES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('ALT URGELL')}>ALT URGELL</DropdownItem>
                    <DropdownItem onClick={() => setComarca('ANOIA')}>ANOIA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAGES')}>BAGES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAIX CAMP')}>BAIX CAMP</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAIX EBRE')}>BAIX EBRE</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAIX EMPORDA')}>BAIX EMPORDA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAIX LLOBREGAT')}>BAIX LLOBREGAT</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BAIX PENEDES')}>BAIX PENEDES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BARCELONES')}>BARCELONES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('BERGUEDA')}>BERGUEDA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('CERDANYA')}>CERDANYA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('CONCA DE BARBERA')}>CONCA DE BARBERA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('GARRIGUES')}>GARRIGUES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('GARROTXA')}>GARROTXA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('GIRONES')}>GIRONES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('MONTSIA')}>MONTSIA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('NOGUERA')}>NOGUERA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('OSONA')}>OSONA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('PALLARS JUSSA')}>PALLARS JUSSA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('PALLARS SOBIRA')}>PALLARS SOBIRA</DropdownItem>
                    <DropdownItem onClick={() => setComarca("PLA D'URGELL")}>PLA D'URGELL</DropdownItem>
                    <DropdownItem onClick={() => setComarca("PLA DE L'ESTANY")}>PLA DE L'ESTANY</DropdownItem>
                    <DropdownItem onClick={() => setComarca('PRIORAT')}>PRIORAT</DropdownItem>
                    <DropdownItem onClick={() => setComarca("RIBERA D'EBRE")}>RIBERA D'EBRE</DropdownItem>
                    <DropdownItem onClick={() => setComarca('RIPOLLES')}>RIPOLLES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('SEGARRA')}>SEGARRA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('SEGRIA')}>SEGRIA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('SELVA')}>SELVA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('SOLSONES')}>SOLSONES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('TARRAGONES')}>TARRAGONES</DropdownItem>
                    <DropdownItem onClick={() => setComarca('TERRA ALTA')}>TERRA ALTA</DropdownItem>
                    <DropdownItem onClick={() => setComarca('URGELL')}>URGELL</DropdownItem>
                    <DropdownItem onClick={() => setComarca("VALL D'ARAN")}>VALL D'ARAN</DropdownItem>
                    <DropdownItem onClick={() => setComarca('VALLES OCCIDENTAL')}>VALLES OCCIDENTAL</DropdownItem>
                    <DropdownItem onClick={() => setComarca('VALLES ORIENTAL')}>VALLES ORIENTAL</DropdownItem>
                    <DropdownItem onClick={() => setComarca("ALTA RIBAGORÃA")}>ALTA RIBAGORÃA</DropdownItem>
                    <DropdownItem onClick={() => setComarca("MOIANÃS")}>MOIANÃS</DropdownItem>
                    <DropdownItem onClick={() => setComarca('PALLARS SOBIRA')}>PALLARS SOBIRA</DropdownItem>
                    
                </DropdownMenu>
            </ButtonDropdown>
            <Table>
                <thead>
                    <th>Data Inici</th>
                    <th>Data Fin</th>
                    <th>Taxa</th>
                    <th>Casos</th>
                </thead>
                <tbody>
                    {filas}
                </tbody>
            </Table>
        </Container>

    );

};

export default Covid;